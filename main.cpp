//#include "mainwindow.h"
#include <QObject>
#include <QApplication>
#include <iostream>
#include <QTimer>

class Printer: public QObject {
    Q_OBJECT
public slots:
    void printerFinished () {
        std::cout<<"Finished"<<std::endl;
    }
};

int main(int argc, char **argv)
{
    QApplication app(argc, argv);
    QTimer myTimer;
    Printer myPrinter;
    QObject::connect(&myTimer,&QTimer::timeout,&myPrinter, &Printer::printerFinished);
    myTimer.start(2000);
    return app.exec();
}
